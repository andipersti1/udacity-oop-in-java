class Main {
    public static void main(String[] args) {
        ContactsManager manager = new ContactsManager();

        Contact peter = new Contact();
        peter.name = "Peter";
        peter.phoneNumber = "1234532";
        manager.addContact(peter);

        Contact hans = new Contact();
        hans.name = "Hans";
        hans.phoneNumber = "345323423";
        manager.addContact(hans);

        Contact klaus = new Contact();
        klaus.name = "Klaus";
        klaus.phoneNumber = "1232434532";
        manager.addContact(klaus);

        Contact wanted = manager.searchContact("Hans");
        if (wanted != null) {
            System.out.println(wanted.phoneNumber);
        }
    }
}
