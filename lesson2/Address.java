import java.util.Scanner;

class Address {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Your address:");
        String address = scanner.nextLine();
        System.out.println("You live at " + address);
    }
}
