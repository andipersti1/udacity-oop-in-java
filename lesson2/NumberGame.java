import java.util.Scanner;

class NumberGame {
    public static void main(String[] args) {
        int randomNumber = (int) (Math.random() * 100) + 1;
        System.out.println("Guess a number between 1 and 100.");

        Scanner scanner = new Scanner(System.in);
        for (int i = 10; i > 0; i--) {
            System.out.println(i + " guess(es) left. Next guess:");
            int guess = scanner.nextInt();

            if (randomNumber < guess) {
                System.out.println("Your guess is too big");
            } else if (randomNumber > guess) {
                System.out.println("Your guess is too small");
            } else {
                System.out.println("Congratulations");
                return;
            }
        }
        System.out.println("The random number was " + randomNumber);
    }
}
