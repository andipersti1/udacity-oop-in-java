import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Game {
	private String movie;
	private String[] movies;
	private String wrongLetters;
	private int wrongGuesses;
	private boolean hasWon;
	private String currentOutput;

	Game(String filename) {
		int count = loadMovies("movies.txt");
		this.movie = this.movies[(int) (Math.random() * count)];
		this.currentOutput = this.movie.replaceAll("\\p{Lower}", "_");
		this.wrongLetters = "";
		this.wrongGuesses = 0;
		this.hasWon = false;
	}

	private int loadMovies(String filename) {
		this.movies = new String[100];
		int lastIndex = 0;

		try {
			File file = new File(filename);
			Scanner scanner = new Scanner(file);

			while (scanner.hasNextLine()) {
				this.movies[lastIndex] = scanner.nextLine();
				lastIndex++;
			}
		} catch (FileNotFoundException e) {
			System.out.println("File doesn't exist");
		}
		return lastIndex;
	}

	public void play() {
		boolean gameFinished = false;
		Scanner scanner = new Scanner(System.in);

		while (!gameFinished) {
			this.displayNextTurn();
			System.out.print("Your next guess: ");
			String guess = scanner.nextLine();
			gameFinished = this.checkGuess(guess.charAt(0));
		}	
	    
		if (this.hasWon) {
			System.out.println("Congratulations! You have guessed correctly.");
		} else {
			System.out.println("You don't have any guesses left.");
		}
		System.out.println("The movie title was '" + this.movie + "'.");
	}

	private void displayNextTurn() {
		System.out.println(this.currentOutput);
		System.out.print("You've guessed (" + this.wrongGuesses + ") wrong letters: ");
		for (int i = 0; i < this.wrongGuesses; i++) {
			System.out.print(this.wrongLetters.charAt(i) + " ");
		}
		System.out.println();
	}

	private boolean checkGuess(char guess) {
		int letterIndex = this.movie.indexOf(guess);
		if (letterIndex >= 0) {
			do {
				char[] temp = this.currentOutput.toCharArray();
				temp[letterIndex] = guess;
				this.currentOutput = new String(temp);
				letterIndex = this.movie.indexOf(guess, letterIndex + 1);
			} while (letterIndex >= 0);

			if (this.currentOutput.equals(this.movie)) {
				this.hasWon = true;
				return true;
			} else {
				return false;
			}
		} else {
			if (this.wrongLetters.indexOf(guess) == -1) {
				this.wrongLetters += guess;
				this.wrongGuesses++;
			}
			if (this.wrongGuesses < 10) {
				return false;
			} else {
				return true;
			}
		}
	}
}
