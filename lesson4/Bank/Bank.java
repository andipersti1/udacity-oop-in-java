public class Bank {
	public static void main (String args[]) {
		CheckingAccount checking = new CheckingAccount();
		SavingsAccount savings = new SavingsAccount();
		COD cod = new COD();

		checking.account = "ab123";
		savings.balance = 123.12;
		cod.account = "bd142";

		System.out.println("Checking account: " + checking.account);
		System.out.println("Savings balance: " + savings.balance);
		System.out.println("COD account: " + cod.account);
	}
}
