public class Chess {
	public static void main(String[] args) {
		Game game = new Game();
		Rock rock = new Rock(1, 2);
		Bishop bishop = new Bishop(2, 5);
		Queen queen = new Queen(3, 3);

		System.out.println(queen.isValidMove(new Position(2, 2)));
		System.out.println(queen.isValidMove(new Position(4, 4)));
		System.out.println(queen.isValidMove(new Position(2, 4)));
		System.out.println(queen.isValidMove(new Position(4, 2)));
		System.out.println(queen.isValidMove(new Position(2, 3)));
		System.out.println(queen.isValidMove(new Position(3, 2)));
		System.out.println(queen.isValidMove(new Position(4, 3)));
		System.out.println(queen.isValidMove(new Position(3, 4)));
		System.out.println(queen.isValidMove(new Position(1, 6)));
		System.out.println(queen.isValidMove(new Position(1, 8)));
	}
}
