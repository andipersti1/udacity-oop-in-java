public class Piece {
	Position position;

	Piece(int r, int c) {
		this.position = new Position(r, c);
	}

	boolean isValidMove(Position newPosition) {
		if (newPosition.row > 0 && newPosition.column > 0 &&
		    newPosition.row < 8 && newPosition.column < 8) {
			return true;
		} else {
			return false;
		}
	}
}
