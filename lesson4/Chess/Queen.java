public class Queen extends Piece {
	Queen(int r, int c) {
		super(r, c);
	}

	boolean isValidMove(Position newPosition) {
		if (!super.isValidMove(newPosition)) {
			return false;
		}

		if (newPosition.row == this.position.row ||
			newPosition.column == this.position.column ||
		    (Math.abs(newPosition.row - this.position.row) ==
		     Math.abs(newPosition.column - this.position.column))) {
			return true;
		} else {
			return false;
		}
	}
}
