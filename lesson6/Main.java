import java.util.ArrayList;

public class Main {
	public static void main (String[] args) {
		Simulation simulation = new Simulation();

		ArrayList<Item> phase1 = simulation.loadItems("phase-1.txt");
		ArrayList<Item> phase2 = simulation.loadItems("phase-2.txt");
		ArrayList<Rocket> rockets;

		System.out.println("Phase 1:");
		rockets = simulation.loadU1(phase1);
		System.out.println("Total cost for U1: " + simulation.runSimulation(rockets) + "m $");
		rockets = simulation.loadU2(phase1);
		System.out.println("Total cost for U2: " + simulation.runSimulation(rockets) + "m $");

		System.out.println("Phase 2:");
		rockets = simulation.loadU1(phase2);
		System.out.println("Total cost for U1: " + simulation.runSimulation(rockets) + "m $");
		rockets = simulation.loadU2(phase2);
		System.out.println("Total cost for U2: " + simulation.runSimulation(rockets) + "m $");
	}
}
