import java.util.ArrayList;

public class Rocket implements SpaceShip {
	protected int cost;
	protected int weight;
	protected int maxWeight;
	protected ArrayList<Item> cargo;

	Rocket(int cost, int weight, int maxWeight) {
		this.cost = cost;
		this.weight = weight;
		this.maxWeight = maxWeight;
		this.cargo = new ArrayList<Item>();
	}

	protected int currentWeight() {
		int sum = 0;
		for (Item i : this.cargo) {
			sum += i.getWeight();
		}
		return sum + this.weight;
	}

	protected double getCrashPercentage(double factor) {
		return factor * ((double) this.currentWeight() / this.maxWeight);
	}

	public boolean launch() {
		return true;
	}
	
	public boolean land() {
		return true;
	}

	public boolean canCarry(Item i) {
		return this.currentWeight() + i.getWeight() <= this.maxWeight;
	}

	public void carry(Item i) {
		this.cargo.add(i);
	}

	public int getCost() {
		return this.cost;
	}
}
