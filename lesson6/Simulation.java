import java.io.FileNotFoundException;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Simulation {
	public ArrayList<Item> loadItems(String filename) {
		ArrayList<Item> items = new ArrayList<Item>();
		File file = new File(filename);
		
		try {
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] parts = line.split("=", 2);
				int weight = Integer.parseInt(parts[1]) / 1000;
				Item item = new Item(parts[0], weight);
				items.add(item);
			}
			return items;
		} catch (FileNotFoundException e) {
			System.err.println("File not found!");
			return items;
		}
	}

	public ArrayList<Rocket> loadU1(ArrayList<Item> items) {
		ArrayList<Rocket> rockets = new ArrayList<Rocket>();
		Rocket rocket = new U1();
	    
		for (Item i : items) {
			if (rocket.canCarry(i)) {
				rocket.carry(i);
			} else {
				rockets.add(rocket);
				rocket = new U1();
				rocket.carry(i);
			}
		}
		rockets.add(rocket);

		return rockets;
	}

	public ArrayList<Rocket> loadU2(ArrayList<Item> items) {
		ArrayList<Rocket> rockets = new ArrayList<Rocket>();
		Rocket rocket = new U2();
	    
		for (Item i : items) {
			if (rocket.canCarry(i)) {
				rocket.carry(i);
			} else {
				rockets.add(rocket);
				rocket = new U2();
				rocket.carry(i);
			}
		}
		rockets.add(rocket);

		return rockets;
	}

	public int runSimulation(ArrayList<Rocket> rockets) {
		int totalCost = 0;

		for (Rocket rocket : rockets) {
			while (!(rocket.launch() && rocket.land())) {
				totalCost += rocket.getCost();
			}
			totalCost += rocket.getCost();
		}

		return totalCost;
	}
}
