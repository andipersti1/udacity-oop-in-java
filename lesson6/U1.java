public class U1 extends Rocket {
	U1() {
		super(100, 10, 18);
	}

	public boolean launch() {
		return Math.random() > getCrashPercentage(0.05);
	}

	public boolean land() {
		return Math.random() > getCrashPercentage(0.01);
	}
}
