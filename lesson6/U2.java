public class U2 extends Rocket {
	U2() {
		super(120, 18, 29);
	}

	public boolean launch() {
		return Math.random() > this.getCrashPercentage(0.04);
	}

	public boolean land() {
		return Math.random() > this.getCrashPercentage(0.08);
	}
}
